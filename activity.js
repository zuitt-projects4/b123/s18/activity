
let trainer = {

	name: "Satoshi",
	age: 10,
	address: {
		city:"Pallet town",
		country: "Kanto Region"
	},
	friends:["Misty", "Brock", "May", "Dawn"],
	pokemons: ["Pikachu", "Butterfree", "Pidgeotto", "Bulbasor", "Charizard", "Squirtle"], 
	catch: function(pokemon){
		if(trainer.pokemons.length < 6){
			trainer.pokemons.push(pokemon)
			alert(`Gotcha, ${pokemon}`)
		}else{
			alert(`A trainer should only have ${trainer.pokemons.length} pokemons to carry`)
		}
	}, 
	release: function(){
		if( trainer.pokemons.length > 0){
			trainer.pokemons.pop()
		}else{
			alert(`You have no more pokemons! Catch one first.`)
		}
	}

}


function Pokemon(name, type, level){
	this.name = name
	this.type = type
	this.level = level 
	this.hp = 3 * level
	this.atk = 2.5 * level
	this.def = 2 * level
	this.isFainted = false
	this.tackle = function(pokemon){
		console.log(`${this.name} tackled ${pokemon.name}`)
		if(pokemon.hp > 0 && this.hp > 0){
			pokemon.hp -= this.atk
			this.hp -= pokemon.atk
			if(pokemon.hp < 0){
				pokemon.faint(pokemon)
			}else if(this.hp < 0){
				this.faint(this.name)
			}
		}
	}
	this.faint = function(pokemon){
		alert(`${this.name} has fainted.`)
		pokemon.isFainted = true
	}
}

let pokemon1 = new Pokemon("Pickachu", "Electric" , 10)
let pokemon2 = new Pokemon("Squirtle", "Water" , 8)

console.log(pokemon1)
console.log(pokemon2)
